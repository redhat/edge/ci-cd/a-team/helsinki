.PHONY: dependencies ansible/dependencies unit lint up stage/up prod/up tools/image/build image/build

ifneq (, $(shell which podman))
CONTAINER_ENGINE := podman
else
CONTAINER_ENGINE := docker
endif
TOOLS_IMAGE=helsinki-tools
in_container = ${CONTAINER_ENGINE} run --rm -it ${TOOLS_IMAGE} make env=$1 $2

dependencies:
	pip install --upgrade pip
	pip install -r requirements.txt
	pip install -r tests/requirements.txt


unit: dependencies
	PYTHONPATH="$PYTHONPATH:./helsinki" pytest --cov=helsinki --cov-report \
		term-missing --cov-fail-under=0 tests/unit

lint: dependencies
	pre-commit run --all-files

ansible/dependencies:
	pip install --upgrade pip
	pip install -r deployment/requirements.txt

up:
	ansible-playbook deployment/main.yml -e env=$(env)

image/build:
	${CONTAINER_ENGINE} build -t helsinki .

tools/image/build:
	${CONTAINER_ENGINE} build -t ${TOOLS_IMAGE} -f deployment/Dockerfile .

dev/up: image/build
	${CONTAINER_ENGINE} run -it helsinki

stage/up: tools/image/build
	$(call in_container,stage,up)

prod/up: tools/image/build
	$(call in_container,prod,up)
